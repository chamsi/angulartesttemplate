import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { PricingComponent } from './pricing/pricing.component';
import { PricingWrapperComponent } from './pricing-wrapper/pricing-wrapper.component';
import { TeamComponent } from './team/team.component';
import { TeamQuotesComponent } from './team-quotes/team-quotes.component';
import { AboutComponent } from './about/about.component';
import { About1Component } from './about1/about1.component';
import { ProjectsComponent } from './projects/projects.component';
import { ArticlesComponent } from './articles/articles.component';
import { ProgresscycleComponent } from './progresscycle/progresscycle.component';
import { WhoWeAre1Component } from './who-we-are1/who-we-are1.component';
import { WhoWeAre2Component } from './who-we-are2/who-we-are2.component';
import { HttpClientModule } from '@angular/common/http';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routerOptions: ExtraOptions = {
  scrollPositionRestoration: 'enabled',
  anchorScrolling: 'enabled',
  scrollOffset: [0, 64],
};

const appRoutes : Routes=[

 { path: 'home', component: HomeComponent },
 { path: 'whoweare1', component: WhoWeAre1Component },
 { path: 'whoweare2', component: WhoWeAre2Component },
 { path: '', redirectTo:'/home', pathMatch: 'full'}, 
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PricingComponent,
    PricingWrapperComponent,
    TeamComponent,
    TeamQuotesComponent,
    AboutComponent,
    About1Component,
    ProjectsComponent,
    ArticlesComponent,
    
    ProgresscycleComponent,
    WhoWeAre1Component,
    WhoWeAre2Component,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes,routerOptions)

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
