import { Component, ElementRef, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private elRef: ElementRef){ 
  
  }

  ngOnInit(){
      $.getScript('/assets/js/plugins.js');
      $.getScript('/assets/js/theme.js'); 
  }

  observer!: MutationObserver;

  ngAfterViewInit(){
    this.observer = new MutationObserver(mutations => {
      
      console.log('Dom change detected...');
      $.getScript('/assets/js/plugins.js');
      $.getScript('/assets/js/theme.js'); 

    });
    var config = { attributes: true, childList: true, characterData: true };

    this.observer.observe(this.elRef.nativeElement, config);
  }
}
