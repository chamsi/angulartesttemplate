import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PricingWrapperComponent } from './pricing-wrapper.component';

describe('PricingWrapperComponent', () => {
  let component: PricingWrapperComponent;
  let fixture: ComponentFixture<PricingWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PricingWrapperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PricingWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
