import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgresscycleComponent } from './progresscycle.component';

describe('ProgresscycleComponent', () => {
  let component: ProgresscycleComponent;
  let fixture: ComponentFixture<ProgresscycleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgresscycleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgresscycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
