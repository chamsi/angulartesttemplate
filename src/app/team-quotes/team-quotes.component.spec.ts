import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamQuotesComponent } from './team-quotes.component';

describe('TeamQuotesComponent', () => {
  let component: TeamQuotesComponent;
  let fixture: ComponentFixture<TeamQuotesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamQuotesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamQuotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
